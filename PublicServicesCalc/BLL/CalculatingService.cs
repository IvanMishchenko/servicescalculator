using System;
using System.Collections.Generic;
using System.Linq;
using PublicServicesCalc.Data;
using PublicServicesCalc.Entities;
using PublicServicesCalc.Helpers;

namespace PublicServicesCalc.BLL
{
    class CalculatingService
    {
        private readonly Repository<LogEntry> _logRepository;
        private readonly SettingsService _settingsService;

        public CalculatingService()
        {
            _logRepository = new Repository<LogEntry>(Constants.ConnectionString);
            _settingsService = new SettingsService();
        }

        public List<LogEntry> All()
        {
            return _logRepository.All().OrderByDescending(x => x.DateCreated).ToList();
        }

        public float Calculate(float counterValue, TypeService type)
        {
            var previousEntry = GetLast(type);

            if (previousEntry == null)
                throw new ArgumentException("You should specify started counter's value");


            var difference = counterValue - previousEntry.CounterValue;

            float result = default(float);
            if (type == TypeService.Electricity)
            {
                var price = _settingsService.GetPrices();
                difference -= 100;
                result += 100 * price.ElectricityBefore100;
                result += difference * price.ElectricityAfter100;
            }
            else
            {
                result += difference * _settingsService.GetPrices(type);
            }

            _logRepository.Create(new LogEntry
            {
                CounterValue = (int)counterValue,
                Price = result,
                TypeService = type
            });

            return result;
        }

        public LogEntry GetLast(TypeService type)
        {
            return _logRepository.All().Where(x => x.TypeService == type)
                .OrderByDescending(x => x.DateCreated).FirstOrDefault();
        }

        public void Write(LogEntry entry)
        {
            _logRepository.Create(entry);
        }
    }
}