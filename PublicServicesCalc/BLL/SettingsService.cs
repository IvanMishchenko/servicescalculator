using System.Linq;
using PublicServicesCalc.Data;
using PublicServicesCalc.Entities;
using PublicServicesCalc.Helpers;

namespace PublicServicesCalc.BLL
{
    internal class SettingsService
    {
        private readonly Repository<Price> _pricesRepository;

        public SettingsService()
        {
            _pricesRepository = new Repository<Price>(Constants.ConnectionString);
        }

        public void Save(Price prices)
        {
            _pricesRepository.Update(prices);
        }

        public float GetPrices(TypeService type)
        {
            var prices = GetPrices();

            switch (type)
            {
                case TypeService.Gas:
                    return prices.Gas;
                case TypeService.Water:
                    return prices.Water;
            }

            return 0;
        }

        public Price GetPrices()
        {
            return _pricesRepository.All().FirstOrDefault();
        }
    }
}