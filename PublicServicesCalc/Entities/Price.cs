namespace PublicServicesCalc.Entities
{
    class Price : Entity
    {
        public float ElectricityBefore100 { get; set; }

        public float ElectricityAfter100 { get; set; }

        public float Water { get; set; }

        public float Gas { get; set; }
    }
}