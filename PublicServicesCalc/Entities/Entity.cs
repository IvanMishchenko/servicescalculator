using System;
using SQLite;

namespace PublicServicesCalc.Entities
{
    public class Entity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }
    }
}