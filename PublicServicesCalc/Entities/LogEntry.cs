namespace PublicServicesCalc.Entities
{
    class LogEntry : Entity
    {
        public TypeService TypeService { get; set; }

        public int CounterValue { get; set; }

        public float Price { get; set; }
    }
}