namespace PublicServicesCalc.Entities
{
    internal enum TypeService
    {
        Electricity,
        Water,
        Gas
    }
}