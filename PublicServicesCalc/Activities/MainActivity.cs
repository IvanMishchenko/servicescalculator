﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Android.App;
using Android.OS;
using Android.Widget;
using PublicServicesCalc.BLL;
using PublicServicesCalc.Entities;
using PublicServicesCalc.Helpers;

namespace PublicServicesCalc.Activities
{
    [Activity(Label = "Service Calculator", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private CalculatingService _calculatingService;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            var calculateButton = FindViewById<Button>(Resource.Id.calculateButton);
            calculateButton.Click += CalculateButtonHandler;

            var settingsButton = FindViewById<Button>(Resource.Id.settingsButton);
            settingsButton.Click += (sender, args) =>
            {
                StartActivity(typeof(Settings));
            };

            var historyButton = FindViewById<Button>(Resource.Id.historyButton);
            historyButton.Click += (sender, args) =>
            {
                StartActivity(typeof (History));
            };
        }

        private void CalculateButtonHandler(object sender, EventArgs eventArgs)
        {
            _calculatingService = new CalculatingService();

            var servisesEditArray = new List<EditText>
            {
                FindViewById<EditText>(Resource.Id.editElecticityValue),
                FindViewById<EditText>(Resource.Id.editGasValue),
                FindViewById<EditText>(Resource.Id.editWaterValue)
            };


            foreach (var editTextObj in servisesEditArray)
            {
                if (!Validator.ValidateFloatEditText(editTextObj))
                {
                    var alertDialog = new AlertDialog.Builder(this);
                    alertDialog.SetMessage($"Please, fill {editTextObj.Tag} field with correct value");
                    alertDialog.Create().Show();

                    return;
                }
            }

            var priceForElectricity = _calculatingService.Calculate(float.Parse(servisesEditArray[0].Text),
                TypeService.Electricity);
            var priceForGas = _calculatingService.Calculate(float.Parse(servisesEditArray[1].Text), TypeService.Gas);
            var pricesForWater = _calculatingService.Calculate(float.Parse(servisesEditArray[2].Text), TypeService.Water);

            var viewResult = FindViewById<TextView>(Resource.Id.textResult);
            viewResult.Text += $"\n Price for electricity: {priceForElectricity.ToString("C", new CultureInfo("UK-ua"))}";
            viewResult.Text += $"\n Price for water: {pricesForWater.ToString("C", new CultureInfo("UK-ua"))}";
            viewResult.Text += $"\n Price for gas: {priceForGas.ToString("C", new CultureInfo("UK-ua"))}";
        }
    }
}

