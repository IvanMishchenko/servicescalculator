using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Widget;
using PublicServicesCalc.BLL;
using PublicServicesCalc.Entities;
using PublicServicesCalc.Helpers;

namespace PublicServicesCalc.Activities
{
    [Activity(Label = "Settings")]
    public class Settings : Activity
    {
        private readonly SettingsService _settingService = new SettingsService();
        private readonly CalculatingService _calculatingService = new CalculatingService();
        private Price _price;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Settings);

            _price = _settingService.GetPrices();

            if (_price != null)
            {
                var priceBefore100 = FindViewById<EditText>(Resource.Id.electricityBefore100Price);
                var priceAfter100 = FindViewById<EditText>(Resource.Id.electricityAfter100Price);
                var priceWater = FindViewById<EditText>(Resource.Id.waterPrice);
                var priceGas = FindViewById<EditText>(Resource.Id.gasPrice);

                priceBefore100.Text = _price.ElectricityBefore100.ToString();
                priceAfter100.Text = _price.ElectricityAfter100.ToString();
                priceWater.Text = _price.Water.ToString();
                priceGas.Text = _price.Gas.ToString();

                var startElectricity = FindViewById<EditText>(Resource.Id.electricityStart);
                var startWater = FindViewById<EditText>(Resource.Id.waterStart);
                var startGas = FindViewById<EditText>(Resource.Id.gasStart);

                startElectricity.Text = _calculatingService.GetLast(TypeService.Electricity).CounterValue.ToString();
                startWater.Text = _calculatingService.GetLast(TypeService.Water).CounterValue.ToString();
                startGas.Text = _calculatingService.GetLast(TypeService.Gas).CounterValue.ToString();
            }

            var saveButton = FindViewById<Button>(Resource.Id.saveButton);
            saveButton.Click += SaveButtonHandler;
        }

        private void SaveButtonHandler(object sender, EventArgs eventArgs)
        {
            var servisesEditArray = new List<EditText>
            {
                FindViewById<EditText>(Resource.Id.electricityBefore100Price),
                FindViewById<EditText>(Resource.Id.electricityAfter100Price),
                FindViewById<EditText>(Resource.Id.waterPrice),
                FindViewById<EditText>(Resource.Id.gasPrice)
            };


            foreach (var editTextObj in servisesEditArray)
            {
                if (!Validator.ValidateFloatEditText(editTextObj))
                {
                    var alertDialog = new AlertDialog.Builder(this);
                    alertDialog.SetMessage($"Please, fill {editTextObj.Tag} field with correct value");
                    alertDialog.Create().Show();

                    return;
                }
            }

            if (_price == null)
            {
                _price = new Price
                {
                    ElectricityBefore100 = float.Parse(servisesEditArray[0].Text),
                    ElectricityAfter100 = float.Parse(servisesEditArray[1].Text),
                    Water = float.Parse(servisesEditArray[2].Text),
                    Gas = float.Parse(servisesEditArray[3].Text)
                };
            }
            else
            {
                _price.ElectricityBefore100 = float.Parse(servisesEditArray[0].Text);
                _price.ElectricityAfter100 = float.Parse(servisesEditArray[1].Text);
                _price.Water = float.Parse(servisesEditArray[2].Text);
                _price.Gas = float.Parse(servisesEditArray[3].Text);
            }

            _settingService.Save(_price);

            var electricityCounterValue = int.Parse(FindViewById<EditText>(Resource.Id.electricityStart).Text);
            var waterCounterValue = int.Parse(FindViewById<EditText>(Resource.Id.waterStart).Text);
            var gasCounterValue = int.Parse(FindViewById<EditText>(Resource.Id.gasStart).Text);

            _calculatingService.Write(new LogEntry
            {
                CounterValue = electricityCounterValue,
                TypeService = TypeService.Electricity
            });
            _calculatingService.Write(new LogEntry
            {
                CounterValue = waterCounterValue,
                TypeService = TypeService.Water
            });
            _calculatingService.Write(new LogEntry
            {
                CounterValue = gasCounterValue,
                TypeService = TypeService.Gas
            });


            StartActivity(typeof(MainActivity));
        }
    }
}