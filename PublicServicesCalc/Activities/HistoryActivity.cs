using System.Globalization;
using Android.App;
using Android.OS;
using Android.Widget;
using PublicServicesCalc.BLL;

namespace PublicServicesCalc.Activities
{
    [Activity(Label = "History")]
    public class History : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.History);


            var calculatingService = new CalculatingService();

            var allEntries = calculatingService.All();

            var textView = FindViewById<TextView>(Resource.Id.historyTextView);

            int i = 1;
            foreach (var entry in allEntries)
            {
                textView.Text +=
                    $"\nFor {entry.TypeService} with counter value '{entry.CounterValue}' was paid {entry.Price.ToString("C", new CultureInfo("UK-ua"))} at {entry.DateCreated.ToShortDateString()}";
                if ((++i & 3) == 0)
                {
                    textView.Text += "\n----------------------------------------------";
                }
            }
        }
    }
}