using System;
using System.Collections.Generic;
using System.Linq;
using PublicServicesCalc.Entities;
using SQLite;

namespace PublicServicesCalc.Data
{
    public interface IRepository<T> where T : Entity
    {
        List<T> All();

        T GetById(long id);

        void Create(T item);

        void Remove(long id);

        void Update(T item);
    }

    public class Repository<T> : IRepository<T> where T : Entity, new()
    {
        private readonly string _pathToDatabase;

        public Repository(string pathToDatabase)
        {
            _pathToDatabase = pathToDatabase;

            using (var connection = GetOpenConnection())
            {
                connection.CreateTable<T>();
            }
        }

        public SQLiteConnection GetOpenConnection()
        {
            var connection = new SQLiteConnection(_pathToDatabase);

            return connection;
        }


        public List<T> All()
        {
            using (var connection = GetOpenConnection())
            {
                var table = connection.Table<T>().ToList();

                return table;
            }
        }

        public T GetById(long id)
        {
            using (var connection = GetOpenConnection())
            {
                return connection.Get<T>(id);
            }
        }

        public void Create(T item)
        {
            using (var connection = GetOpenConnection())
            {
                item.DateCreated = DateTime.Now;
                item.DateModified = DateTime.Now;

                connection.Insert(item, typeof(T));
            }
        }

        public void Remove(long id)
        {
            using (var connection = GetOpenConnection())
            {
                connection.Delete<T>(id);
            }
        }

        public void Update(T item)
        {
            using (var connection = GetOpenConnection())
            {
                item.DateModified = DateTime.Now;

                connection.InsertOrReplace(item, typeof(T));
            }
        }
    }
}