using System;
using System.IO;

namespace PublicServicesCalc.Helpers
{
    public static class Constants
    {
        public static string ConnectionString
        {
            get
            {
                var databaseName = "Services.db3";
                var documentPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

                var path = Path.Combine(documentPath, databaseName);

                return path;
            }
        }
    }
}