using Android.Widget;

namespace PublicServicesCalc.Helpers
{
    public static class Validator
    {
        public static bool ValidateFloatEditText(EditText editText)
        {
            var value = editText.Text;

            if (string.IsNullOrEmpty(value))
                return false;

            float floatValue;
            if (!float.TryParse(value, out floatValue))
                return false;

            return true;
        }
    }
}